var dillersSelect, citiessSelect, citiesScroll, dillersScroll,
	animEndEventNames = {
		'WebkitAnimation' : 'webkitAnimationEnd',
		'OAnimation' : 'oAnimationEnd',
		'msAnimation' : 'MSAnimationEnd',
		'animation' : 'animationend'
	},
	animEndEventName = animEndEventNames[ Modernizr.prefixed( 'animation' ) ];
$(function(){
	if ($('.wrapper').hasClass('story-view')) {
		$('.btn-menu').removeClass('js-anim');
	}
	// show block on scroll
	if ($('.story-content')) {
		window.sr = ScrollReveal();
		sr.reveal('.story-content .panel-grid');
	}
	// preloader
	// setTimeout(function(){
	// 	var outClass = 'pt-page-rotateBottomSideFirst',
	// 		inClass = 'pt-page-moveFromBottom pt-page-delay200 pt-page-ontop pt-page-current not-loaded';

	// 	$('.preloader-wrap').addClass( outClass ).on( animEndEventName, function() {
	// 		$('.preloader-wrap').addClass('loaded');
	// 		$('body').removeClass('ovfHid');
	// 	});

	// 	$('.wrapper.main').addClass( inClass ).on( animEndEventName, function() {
	// 		$(this).removeClass( inClass );

	// 		homeAnimPlayed = true;
	// 		playHomeAnim();
	// 	});

	// 	if ($('.wrapper').hasClass('main')) {
	// 		var video = document.getElementById("bgvid");
	// 		video.play();
	// 	}

	// }, 5000);

	// localStorage.setItem('load', true);
	// console.log(localStorage.getItem('load'));
	console.log(localStorage.getItem('load'));
	if (localStorage.getItem('load') == 'loaded') {
		$('.preloader-wrap').addClass('loaded');
		$('body').removeClass('ovfHid');
		var outClass = 'pt-page-rotateBottomSideFirst',
			inClass = 'pt-page-moveFromBottom pt-page-delay200 pt-page-ontop pt-page-current not-loaded';

			$('.wrapper.main').addClass( inClass ).on( animEndEventName, function() {
				$(this).removeClass( inClass );

				homeAnimPlayed = true;
				playHomeAnim();
			});

			if ($('.wrapper').hasClass('main')) {
				var video = document.getElementById("bgvid");
				video.play();
			}
	} else {
		$('.preloader-wrap').removeClass('hidden');
		setTimeout(function(){
			var outClass = 'pt-page-rotateBottomSideFirst',
				inClass = 'pt-page-moveFromBottom pt-page-delay200 pt-page-ontop pt-page-current not-loaded';

			$('.preloader-wrap').addClass( outClass ).on( animEndEventName, function() {
				$('.preloader-wrap').addClass('loaded');
				$('body').removeClass('ovfHid');
			});

			$('.wrapper.main').addClass( inClass ).on( animEndEventName, function() {
				$(this).removeClass( inClass );

				homeAnimPlayed = true;
				playHomeAnim();
			});

			if ($('.wrapper').hasClass('main')) {
				var video = document.getElementById("bgvid");
				video.play();
			}

		}, 5000);
		localStorage.setItem('load', 'loaded');
	}


	$('.js-menu').on('click', function() {
		$(this).toggleClass('opened');
		$('.menu').toggleClass('opened');
		$('body').toggleClass('ovfHid overlay-open');
	});

	$('.js-success').on('click', function(e) {
		$('.success').addClass('hidden');
		$('#test-drive-form')[0].reset();
		$('.test-drive-form').removeClass('hidden');
		//clear dropdowns
		var dillerSelect = $('input[name=diller]'),
			citySelect = $('input[name=city]');

		dillerSelect.parent().children('span').removeClass('selected').text('Дилер');
		dillerSelect.parent().parent().removeClass('selected');
		dillerSelect.val('-1');

		citySelect.parent().children('span').removeClass('selected').text('Місто');
		citySelect.parent().parent().removeClass('selected');
		citySelect.val('-1');
	});

	$('.js-submit').on('click', function() {
		validateDropdown();
	});

	$.validator.methods.letters = function(value, element, param) {
		return this.optional( element ) || /^[a-zA-Zа-яА-Яіїє]+$/.test( value );
	};

	$.validator.addMethod( "phoneUA", function( phone_number, element ) {
		return this.optional( element ) || /^\+ ?38 ?\([0-9]{3}\) ?[0-9]{3} ?[0-9]{2} ?[0-9]{2}$/.test( phone_number );
	}, "Перевір формат: + 38 (099) 123 34 45" );


	$('#test-drive-form').validate({
		rules: {
			name: {
				required: true,
				// letters: true
			},
			surname: {
				required: true,
				// letters: true
			},
			phone: {
				required: true,
				phoneUA: true
			},
			email: {
				required: true,
				email: true
			},
			city: {
				required: true
			},
			diller: {
				required: true
			}
		},
		messages: {
			name: {
				required: 'Заповни це поле',
				letters: 'Ім’я може містити тільки літери'
			},
			surname: {
				required: 'Заповни це поле',
				letters: 'Прізвище може містити тільки літери'
			},
			phone: {
				required: 'Заповни це поле',
				phoneUA: 'Перевір формат: + 38 (099) 123 34 45'
			},
			email: {
				required: 'Заповни це поле',
				email: 'Перевір формат'
			},
		},
		submitHandler: function(form) {
			// e.preventDefault();
			if (validateDropdown()) {
				var data=$("#test-drive-form").serialize();
				 $.ajax({
					 type: 'POST',
					 url: "/wp-admin/admin-ajax.php",
					 data: "action=get_contact&" + data,
					 cache: false,
					 crossDomain: true,
					 xhrFields: {
						 withCredentials: true
					 },
					success: function() {
						$('.test-drive-form').addClass('hidden');
						$('.success').removeClass('hidden');
					},
					error: function(resp) {
						console.log(resp);
					}
				 });
			}
		}
	});


	if ($( '#cd-dropdown' ).length) {
		citiessSelect = $( '#cd-dropdown' ).dropdown({
			gutter : 5,
			stack : false,
			slidingIn : 100,
			onOptionSelect: function() {
				$('input[name=city]').addClass('selected');
				$('input[name=city]').prev().parent().parent().addClass('selected');
			},
			afterOptionSelect: function() {
				var city = $('input[name=city]').val();
				setCurrentDillers(city);
			}
		});
		citiessSelect.open();
		citiessSelect.listopts.jScrollPane();
		citiessSelect.close();
	}

	if ($( '#cd-dropdown-dillers' ).length) {
		dillersSelect = $( '#cd-dropdown-dillers' ).dropdown({
			gutter : 25,
			stack : false,
			slidingIn : 100,
			onOptionSelect: function() {
				$('input[name=diller]').prev().addClass('selected');
				$('input[name=diller]').prev().parent().parent().addClass('selected');
			},
			afterLoad: function() {;
				$('input[name=diller]').parent().addClass('unactive');
			},
			afterOpen: function() {
				// if ( dillersScroll ) {
				// 	dillersScroll.data('jsp').reinitialise();
				// }
			}
		});
		// dillerSelect
		dillersSelect.open();
		dillersScroll = dillersSelect.listopts.jScrollPane();
		dillersSelect.close();
	}

	// animated inputs
	(function() {
		// trim polyfill : https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/Trim
		if (!String.prototype.trim) {
			(function() {
				// Make sure we trim BOM and NBSP
				var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
				String.prototype.trim = function() {
					return this.replace(rtrim, '');
				};
			})();
		}

		[].slice.call( document.querySelectorAll( 'input.form-control' ) ).forEach( function( inputEl ) {
			// in case the input is already filled..
			if( inputEl.value.trim() !== '' ) {
				classie.add( inputEl.parentNode, 'input--filled' );
			}

			// events:
			inputEl.addEventListener( 'focus', onInputFocus );
			inputEl.addEventListener( 'blur', onInputBlur );
		} );

		function onInputFocus( ev ) {
			classie.add( ev.target.parentNode, 'input--filled' );
		}

		function onInputBlur( ev ) {
			if( ev.target.value.trim() === '' ) {
				classie.remove( ev.target.parentNode, 'input--filled' );
			}
		}
	})();

	// init input mask
	// $('#input-phone').on('input', function(){
	// 	console.log();
	// });
	// set base phone value
	$('#input-phone').val('+38 0');
	$('#input-phone').inputmask({
		"mask": "+99 (999) 999 99 99",
		onBeforePaste: function (pastedValue, opts) {
			console.log('pastedValue');
		}
	});

	$('.js-btn-animation').hover(
		function(){
			var lines = $(this).find('.line');
			$.each(lines, function(i, el){
				if (i%2 == 0) {
					$(el).css({'transform': ' translate(' + randomBetween(3, 7) + '%, 0)'});
				} else {
					$(el).css({'transform': ' translate(-' + randomBetween(5, 9) + '%, 0)'});
				}
			});
		},
		function(){
			var lines = $(this).find('.line');
			$.each(lines, function(i, el){
				$(el).css({'transform': ' translate( 0, 0)'});
			});
		}
	);
});
function randomBetween(min, max) {
	if (min < 0) {
		return min + Math.random() * (Math.abs(min)+max);
	} else {
		return min + Math.random() * max;
	}
}
function setCurrentDillers(cityId) {
	var cityId = cityId,
		dillerSelect = $('input[name=diller]'),
		dillers = dillerSelect.next('ul').find('li'),
		dillersObject = {};

	if (dillersSelect.isOpen()) {
		dillersSelect.close();
	}

	$.each(dillers, function(i, el){
		if ($(el).data('city') != cityId ) {
			$(el).addClass('hidden');
		} else {
			$(el).removeClass('hidden');
		}
	});

	// delete erroe message
	dillerSelect.parent().find('label.dropdown-error').remove();
	dillerSelect.prev().removeClass('error');

	// !TOFIX right metho to clear selected value
	var activeEls = $('input[name=diller]').next('ul').find('li').not('.hidden');
	if (activeEls.length > 1) {

		dillerSelect.parent().removeClass('unactive');
		// clear selected value
		dillerSelect.parent().children('span').removeClass('selected').text('Дилер');
		dillerSelect.parent().parent().removeClass('selected');
		dillerSelect.val('-1');

	} else if (activeEls.length == 1) {

		dillerSelect.parent().addClass('unactive');
		dillersSelect.chooseEl($(activeEls));
	} else {

		dillerSelect.parent().addClass('unactive');
		dillerSelect.val('-1');
		dillerSelect.parent().children('span').removeClass('selected').text('Дилер');
		dillerSelect.parent().parent().removeClass('selected');
	}

	// !TOFIX write metod
	// $('input[name=diller]').next().addClass('hidden');
	dillersSelect.open();
	dillersScroll.data('jsp').reinitialise();
	dillersSelect.close();
	// $('input[name=diller]').next().removeClass('hidden');
}
function validateDropdown() {
	var iCity = $('input[name=city]'),
		iDiller = $('input[name=diller]'),
		cityIsValid,
		dillerIsValid;

	if (iCity.val() == '-1') {
		iCity.prev().addClass('error');
		if (!iCity.parent().find('.dropdown-error').length) {
			iCity.parent().append('<label class="dropdown-error">Обери дилера</label>')
		}
		cityIsValid = false;
	} else {
		iCity.prev().removeClass('error');
		cityIsValid = true;
	}

	if (iDiller.val() == '-1') {
		iDiller.prev().addClass('error');

		if (!iDiller.parent().find('.dropdown-error').length) {
			iDiller.parent().append('<label class="dropdown-error">Обери дилера</label>')
		}
		dillerIsValid = false;
	} else {
		iDiller.prev().removeClass('error');
		dillerIsValid = true;
	}

	if (cityIsValid && dillerIsValid) {
		return true;
	} else {
		return false;
	}
}