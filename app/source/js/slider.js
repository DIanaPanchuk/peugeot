var slider,
	isAnimating = false,
	homeAnimPlayed = false,
	storyAnimPlayed = false,
	rulesAnimPlayed = false,
	testDriveAnimPlayed = false,
	md = new MobileDetect(window.navigator.userAgent);
$(function(){
	var $pages = $( 'div.pt-page.section' ),
		current = 0,
		currentIndex = 0,
		newCurrentIndex = 0,
		endCurrPage = false,
		endNextPage = false,
		animEndEventNames = {
			'WebkitAnimation' : 'webkitAnimationEnd',
			'OAnimation' : 'oAnimationEnd',
			'msAnimation' : 'MSAnimationEnd',
			'animation' : 'animationend'
		},
		animEndEventName = animEndEventNames[ Modernizr.prefixed( 'animation' ) ],
		support = Modernizr.cssanimations;

	if (md.tablet()) {
		$('body').addClass('tablet');
	}

	var w = $(document).height();
	slider = $(".slider").lightSlider({
		vertical: true,
		item: 1,
		mode: "fade",
		speed: 0,
		pager: false,
		controls: false,
		verticalHeight: w,
		enableTouch: false,
		enableDrag: false,
		onAfterSlide: function (el) {

			var currentPage = el.getCurrentSlideCount();

			var active = currentIndex;
			var newActive = slider.getCurrentSlideCount()-1;

			newCurrentIndex = slider.getCurrentSlideCount()-1;


			if (newActive > active) {
				var animationDirection = 'up';
			} else {
				var animationDirection = 'down';
			}

			if( isAnimating ) {
				return false;
			}

			isAnimating = true;

			var $currPage = $pages.eq(active);
			var $nextPage = $pages.eq(newActive).addClass( 'pt-page-current' ),
							outClass = '', inClass = '';

			switch( animationDirection ) {
				case 'up':
					outClass = 'pt-page-rotateBottomSideFirst';
					inClass = 'pt-page-moveFromBottom pt-page-delay200 pt-page-ontop';
					break;
				case 'down':
					outClass = 'pt-page-rotateTopSideFirst';
					inClass = 'pt-page-moveFromTop pt-page-delay200 pt-page-ontop'
					break;
			}

			$currPage.addClass( outClass ).on( animEndEventName, function() {
				$currPage.off( animEndEventName );
				endCurrPage = true;
				if( endNextPage ) {
					onEndAnimation( $currPage, $nextPage );
				}
			} );

			$nextPage.addClass( inClass ).on( animEndEventName, function() {
				$nextPage.off( animEndEventName );
				endNextPage = true;
				if( endCurrPage ) {
					onEndAnimation( $currPage, $nextPage );
				}
			});

			if( !support ) {
				onEndAnimation( $currPage, $nextPage );
			}

			currentIndex = slider.getCurrentSlideCount()-1;

			//show images
			if(currentPage === 2 && !storyAnimPlayed) {
				playStoriesAnim();
				storyAnimPlayed = true;
			}

			if(currentPage === 3 && !rulesAnimPlayed) {
				playRulesAnim();
				rulesAnimPlayed = true;
			}

			if(currentPage === 4 && !testDriveAnimPlayed) {
				playTestDriveAnim();
				testDriveAnimPlayed = true;
			}
		},
		onSliderLoad: function (el) {
			// init title animation
			initTitleAnim();
			//init stories mobile carousel
			initMobile();
			checkActiveScreen('load');

			if (md.tablet()) {
				$('body').addClass('-stage_start');
			}
		}
	});

	function init() {
		$('div.section').removeClass('active');
		$pages.each( function() {
			var $page = $(this);
			$page.data( 'originalClassList', $page.attr( 'class' ) );
		});
		$pages.eq( current ).addClass( 'pt-page-current active' );

	}

	init();

	function onEndAnimation( $outpage, $inpage ) {
		endCurrPage = false;
		endNextPage = false;
		resetPage( $outpage, $inpage );
		isAnimating = false;
	}
	function resetPage( $outpage, $inpage ) {
		$outpage.attr( 'class', $outpage.data( 'originalClassList' ) );
		$inpage.attr( 'class', $inpage.data( 'originalClassList' ) + ' pt-page-current active' );
	}

	if ($('.main').length) {
		// init mouse wheel navigation
		$(window).on('mousewheel', _.debounce(function(event) {

			if (event.deltaY > 0 && $(document).width() >=970 && !isAnimating && !md.tablet()) {
				// from top to bottom
				slider.goToPrevSlide();

			} else if (event.deltaY < 0 && $(document).width() >=970 && !isAnimating && !md.tablet()) {
				// from bottom to top
				slider.goToNextSlide();
			}
		}, 100));
	}

	// menu navigation
	$('#menu').on('click', 'a', function(e){
		var screen = $(this).data('page');
		e.stopPropagation();

		if ($('.main').length && screen != $('.pt-page-current').attr('id')) {
			closeMenu();
			e.preventDefault();
			if ($(document).width() < 970 && !md.tablet() || md.tablet()) {
				setTimeout(function(){
					checkActiveScreen('menu', screen);
				}, 500);
			} else if (!isAnimating) {
				switchSlide(screen);
			}
		} else if ($('.pt-page-current').attr('id') == 'home' && $('body').hasClass('tablet')) {
			closeMenu();
			$('html, body').stop().animate({
				'scrollTop': 0
			}, 1500, 'swing');
		} else if (screen = $('.pt-page-current').attr('id')) {
			closeMenu();
		} else {
			return true;
		}
	});

	// navigation to test-drive
	$('.js-btn-enroll').on('click', function(e) {
		var page = $(this).data('page');
		if ($('.main').length) {
			e.preventDefault();
			// TODO if screen less than 970 scroll to section
			// switchSlide(page);

			if ($(document).width() < 970 || md.tablet()) {
				setTimeout(function(){
					checkActiveScreen('menu', 'test-drive');
				}, 500);
			} else if (!isAnimating) {
				switchSlide('test-drive');
			}

		} else {
			return true;
		}
	});
	// logo navigation
	$('.js-btn-home').on('click', function(e) {
		if ($('.main').length) {
			e.preventDefault();
			// TODO if screen less than 970 scroll to section
			switchSlide('home');
		} else {
			return true;
		}
	});
});
function closeMenu() {
	$('.btn-menu').removeClass('opened');
	$('.menu').removeClass('opened');
	$('body').removeClass('ovfHid overlay-open');
}
function checkActiveScreen(mode, screen) {
	var currentLocation = window.location.href,
		locationArray = currentLocation.split("#");

	if ($('.main').length && locationArray.length > 1 || $('.main').length && mode == "menu") {

		var locationUrl = currentLocation.split('#'),
			newId = locationUrl[1]; // id of current screen

		if ($(document).width() < 970 && !md.tablet() || md.tablet()) {

			if (mode == "menu") {
				newScreen = $('#' + screen),
				newPosition = newScreen.position().top; // position of current screen
			} else {
				newScreen = $('#' + newId),
				newPosition = newScreen.position().top; // position of current screen
			}

			$('html, body').stop().animate({
				'scrollTop': newPosition
			}, 1500, 'swing');

		} else if (!isAnimating) {
			if (newId != 'home') {
				switchSlide(newId);
			} else { console.log('clock slide switch'); }
		}
	}
	location.hash = '';
}
function switchSlide(screen) {
	switch(screen) {
		case 'home':
			slider.goToSlide(0);
			break;
		case 'stories':
			slider.goToSlide(1);
			break;
		case 'rules':
			slider.goToSlide(2);
			break;
		case 'test-drive':
			slider.goToSlide(3);
			break;
		default:
			break;
	}
}
function playRulesAnim() {
	setTimeout(function(){
		$('.rules').removeClass('js-anim');
	}, 500);
	setTimeout(function(){
		crashedNumbers();
	}, 3000);
}
function playStoriesAnim() {
	setTimeout(function(){
		$('body').addClass('-stage_start');
		$('.stories').removeClass('js-anim');
	}, 500);
}
function playTestDriveAnim() {
	setTimeout(function(){
		$('.test-drive').removeClass('js-anim');
	}, 500);
}
function playHomeAnim() {
	$('.btn-menu').removeClass('js-anim');
	$('.home').removeClass('js-anim');
}
function initTitleAnim() {
	$("h1 .first").lettering();
	$("h1 .second").lettering();
}