var nib = require('nib'),
    gulp = require('gulp'),
    jade = require('gulp-jade'),
    rimraf = require('gulp-rimraf'),
    stylus = require('gulp-stylus'),
    concat = require('gulp-concat'),
    changed = require('gulp-changed'),
    connect = require('gulp-connect'),
    imagemin = require('gulp-imagemin'),
    compress = require('gulp-yuicompressor'),

    fs = require('fs');

var projectConfig = {
  src: './app/source',
  sourceJS: './app/source/js',
  sourceCss: './app/source/css',
  sourceImg: './app/source/i',
  sourceFonts: './app/source/fonts/*.*',
  sourceVideo: './app/source/video/*.*',
  bowerSrc: './bower_components',
  dest: './app/dest',
  destJS: './app/dest/js',
  destCSS: './app/dest/css',
  destImg: './app/dest/i',
  destFonts: './app/dest/fonts',
  destVideo: './app/dest/video'
};

var jedeFiles = [
  projectConfig.src+'/**/*.jade'
];

//Jade
gulp.task('jade', function() {
  var YOUR_LOCALS = {};
  gulp.src(jedeFiles)
    .pipe(changed(projectConfig.dest+'/', {hasChanged: changed.compareSha1Digest}))
    .pipe(jade({
      pretty: true,
      locals: JSON.parse(fs.readFileSync('./package.json', 'utf8'))
    }))
    .pipe(gulp.dest(projectConfig.dest+'/'))
    .pipe(connect.reload());
});

var stylusFiles = [
  '!node_modules/**/*.styl',
  projectConfig.src+'/stylus/app.styl'
];

//compile & minify Stylus to source/css
gulp.task('stylus', function () {
  gulp.src(stylusFiles)
    .pipe(changed(projectConfig.sourceCss, {hasChanged: changed.compareSha1Digest}))
    .pipe(stylus({use: [nib()]}))
    .pipe(concat('app.css'))
    .pipe(gulp.dest(projectConfig.destCSS))
    .pipe(compress({type: 'css'}))
    .pipe(connect.reload());
});

var copyJS = [
  projectConfig.bowerSrc+'/jquery/dist/jquery.min.js',
  // projectConfig.bowerSrc+'/lightslider/dist/js/lightslider.min.js',
  projectConfig.bowerSrc+'/scrollreveal/dist/scrollreveal.min.js',
  projectConfig.bowerSrc+'/underscore/underscore-min.js',
  projectConfig.bowerSrc+'/mobile-detect/mobile-detect.js',
  projectConfig.bowerSrc+'/Snap.svg/dist/snap.svg-min.js',
  projectConfig.bowerSrc+'/letteringjs/jquery.lettering.js',
  projectConfig.bowerSrc+'/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js',
  './app/source/js/jquery.jscrollpane.min.js',
  './app/source/js/owl.carousel-custom.min.js',
  './app/source/js/classie.js',
  './app/source/js/modernizr.custom.js',

  './app/source/js/stories.js',
  './app/source/js/numbers.js',
  // './app/source/js/modernizr.custom.js',
  './app/source/js/jquery.mousewheel.min.js',
  './app/source/js/jquery.lightSlider.min.js',
  './app/source/js/slider.js',

  './app/source/js/jquery.dropdown.js',
  './app/source/js/jquery.validate.js',
  './app/source/js/velocity.min.js',
  // './app/source/js/home-transition.js',
  './app/source/js/common.js'
  // './app/source/js/**/*.js'
];

//concat js files
gulp.task('js', function(){
  gulp.src(copyJS)
    .pipe(compress({type: 'js'}))
    .pipe(concat('app.min.js'))
    .pipe(gulp.dest(projectConfig.destJS))
    .pipe(connect.reload());
});

// fonts
var copyFonts = [
    './app/source/fonts/*.*'
];

gulp.task('fonts', function() {
    gulp.src(projectConfig.sourceFonts)
        .pipe(gulp.dest(projectConfig.destFonts))
        .pipe(connect.reload());
});

gulp.task('video', function() {
    gulp.src(projectConfig.sourceVideo)
        .pipe(gulp.dest(projectConfig.destVideo))
        .pipe(connect.reload());
});

//img minify
gulp.task('imageMinify', function() {
  gulp.src(projectConfig.sourceImg+'/**')
    .pipe(imagemin({
      progressive: true,
      optimizationLevel: 7
    }))
    .pipe(gulp.dest(projectConfig.destImg))
    .pipe(connect.reload());
});
// move images
gulp.task('image', function() {
    gulp.src(projectConfig.sourceImg+'/**')
        .pipe(gulp.dest(projectConfig.destImg))
        .pipe(connect.reload());
});

//clean dest dir
gulp.task('clean', function () {
  return gulp.src(projectConfig.dest, {read: false})
    .pipe(rimraf());
});

//server
gulp.task('connectDev', function () {
  connect.server({
    root: [projectConfig.dest],
    port: 3000,
    livereload: true
  });
});

//watch
gulp.task('watch', function () {
  gulp.watch(jedeFiles, ['jade']);
  gulp.watch(projectConfig.src+'/stylus/*.styl', ['stylus']);
  gulp.watch(projectConfig.sourceImg+'/**', ['image']);
  // gulp.watch(projectConfig.sourceImg+'/**', ['imageMinify']);
  gulp.watch(projectConfig.sourceJS+'/**/*.js', ['js']);
});
//imageMinify
gulp.task('default', ['jade', 'stylus', 'js', 'image', 'connectDev', 'watch', 'fonts', 'video']);
gulp.task('prod', ['stylus', 'js', 'image', 'fonts', 'video']);